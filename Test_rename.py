"""Unit Test für rename_file()"""
import string
import random
import datetime
import os
import unittest
from main import rename_file

class TestRenameFile(unittest.TestCase):
    '''Unittest für rename_file'''
    def test_rename_file(self):
        """BasisTest"""
        letters = string.ascii_lowercase
        with open(random.choice(letters) + ".txt" ) as file:
            file.close()
        file_path = os.path.abspath(file)
        self.assertEqual( + datetime.datetime.now().strftime('%y' + '%m' + '%d' + '%H') + ".txt", rename_file(file, file_path))
    
if __name__ == '__main__':
    unittest.main()