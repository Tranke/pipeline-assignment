"""Skript zum Austesten der Pipeline"""
import os
# import logging
# import shutil
import pathlib
from datetime import datetime


inoutlog = ("in", "out")
SCRIPTDIR = os.getcwd()
now = datetime.now()
TSTAMP2 = now.strftime('%Y-%m-%d %H:%M:%S')

#Timestamps
def get_tstamp():
    """Gibt einen Zeitstempel aus"""
    tstamp = now.strftime('%y' + '%m' + '%d' + '%H')
    return tstamp

# #rename
def rename_file(datei, pfad):
    """Fügt einen Zeitstempel dem Namen einer Datei hinzu"""
    file_name_wthout_ext = pathlib.Path(pfad).stem
    new_name = file_name_wthout_ext + get_tstamp() + ".txt"
    os.rename(datei.name, new_name)
    return new_name

#create file
def create_file():
    """Erstellt eine Datei "text_.txt" im aktuellen Arbeitsverzeichnis.
       rename_file() wird verwendet.                                    """
    try:
        with open("text_.txt", "w") as txt:
            txt.write("Hallo Welt")
            txt.close()
        txt_pfad = os.getcwd() + "/test/in/" + txt.name
        rename_file(txt, txt_pfad)
    except OSError as error:
        print(error)

create_file()

# #create Archive dirs
# for x in inoutlog:
#     if x != "log":
#         if not os.path.exists(SCRIPTDIR + "/test/arc/" + x + "/" + get_tstamp()):
#             os.makedirs(SCRIPTDIR + "/test/arc/" + x + "/" + get_tstamp())
#     else:
#         if not os.path.exists(SCRIPTDIR + "/" + x):
#             os.makedirs(SCRIPTDIR + "/" +  x)

# #Logging
# logging.basicConfig(level=logging.INFO,
#                     format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
#                     datefmt='%m-%d %H:%M',
#                     filename='Test_' + get_tstamp() + '.log',
#                     filemode='w')
# logger = logging.getLogger(__name__)
# logger.info('Datum: %s', TSTAMP2)
# logger.info(get_tstamp())


# os.chdir(SCRIPTDIR + "/test/in")
# create_file()

# # Backing up files in "/in" to "/arc/in"
# logger.info("Backing up files...")
# dest_dir = SCRIPTDIR + "/test/arc/in/" + get_tstamp()
# src_files = os.listdir(SCRIPTDIR + "/test/in")
# for file_name in src_files:
#     try:
#         full_file_name = os.path.join(SCRIPTDIR + '/test/in', file_name)
#         if os.path.isfile(full_file_name):
#             if get_tstamp() in full_file_name:
#                 shutil.copy(full_file_name,dest_dir)
#                 logging.info('%s has been backed up', full_file_name  )
#     except OSError as error:
#         print(error)
#         logging.info('could not backup %s', full_file_name)
# logging.info("Backup 'in' files finished")
# logging.shutdown()

# #Remove empty directories
# for x in inoutlog:
#     if x != "log":
#         try:
#             os.rmdir(SCRIPTDIR + "/test/arc/" + x + "/" + get_tstamp())
#         except OSError as error:
#             pass

# #Change Log dir
# shutil.move(SCRIPTDIR + "/test_" + get_tstamp() + ".log",
#             SCRIPTDIR + "/logs/test_" + get_tstamp() + ".log"
#             )
            