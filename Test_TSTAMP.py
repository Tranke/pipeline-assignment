"""Unit Test für create TSTAMP"""
import unittest
import datetime
from main import get_tstamp

class TestGetStamp(unittest.TestCase):
    '''Unittest für get_TSTAMP()'''
    def test_get_tstamp(self):
        """Test"""
        self.assertEqual(datetime.datetime.now().strftime('%y' + '%m' + '%d' + '%H'), get_tstamp())
    
if __name__ == '__main__':
    unittest.main()
