"""Unit Test für create create_file()"""
import datetime
import os
import unittest

class TestCreateFile(unittest.TestCase):
    '''Unittest für create_file'''
    def test_create_file(self):
        """BasisTest"""
        self.assertTrue(True, os.path.isfile("text_" + datetime.datetime.now().strftime('%y' + '%m' + '%d' + '%H') + ".txt"))
    
if __name__ == '__main__':
    unittest.main()
